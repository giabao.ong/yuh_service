const jwt = require('jsonwebtoken');
const passport = require("passport");
var UserDB = require("../models/user");
var permissionController = require("../controllers/permissionController");
var moment = require('moment')
var bcrypt = require('bcrypt');
var nodemailer = require('nodemailer');
var postDB = require('../models/post')
var cmtDB = require('../models/comment')

require('dotenv').config();

exports.loginUser = (req, res) => {
    passport.authenticate('local', (err, user, info) => {
        if (err || !user || info) {
            return res.status(400).send({ status: 400, message: info.message });
        }
        req.login(user, (err) => {
            if (err) {
                return res.status(400).send({ status: 400, message: err.message });
            }

            // generate a signed son web token with the contents of user object and return it in the response
            const token = jwt.sign({ _id: user._id }, process.env.SECRET_KEY, {
                expiresIn: "120h"
            });
            return res.status(200).send({ token: token });
        });
    })(req, res);
}

exports.loginAdmin = (req, res) => {
    passport.authenticate('local', (err, user, info) => {
        if (err || !user || info) {
            return res.status(400).send({ status: 400, message: info.message });
        }
        req.login(user, (err) => {
            if (err) {
                return res.status(400).send({ status: 400, message: err.message });
            }

            // generate a signed son web token with the contents of user object and return it in the response
            const token = jwt.sign({ _id: user._id }, process.env.SECRET_KEY, {
                expiresIn: "120h"
            });
            if (user.role == "ADMIN")
                return res.status(200).send({ token: token });
            else
                return res.status(400).send({ status: 400, message: "Không thể truy cập" });
        });
    })(req, res);
}

function UserRegisterValidation(data, cb) {
    UserDB.getFromUsername(data.username, function(err, data) {
        if (err) return cb("Lỗi không xác định, vui lòng đăng ký lại!");
        if (data) return cb("Username này đã được đăng ký, vui lòng sử dụng username khác!");
        cb(null);
    });
}

exports.register = (req, res) => {
    var query = req.body
    var data = {
        username: query.username || "",
        password: query.password || "",
        name: query.name || "",
        age: query.age || 0,
        gender: query.gender,
        email: query.email,
        position: query.position,
        role: query.role,
        bureau: query.bureau || null,
        avatar: query.avatar || "https://image.flaticon.com/icons/png/512/2919/2919573.png",
        phone: query.phone || "",
        _address: query._address || "",
        level: 1,
        experience: 0,
        coin: 0,
        happy: 200,
        skill: [],
        power: 200,
        rank: 0,
        mentor: [],
        trainee: [],
        createdTime: moment().format(),
        tasks: 0,
        missions: 0
    };
    var result = {};
    var status = 200;
    if (data.phone == "" || data._address == "" || data.name == "" || data.age < 18 || data.age > 65) {
        return res.status(400).send({ status: 400, message: "Input invalid, must have phone & address & name & age > 18 & age <= 65" })
    }
    var pass = data.password
    UserRegisterValidation(data, function(msg) {
        if (msg == null) {
            UserDB.create(data, function(err, id) {
                if (err) {
                    console.log("[UserController] Failed to add user to database: " + err);
                    status = 500;
                    result.status = status;
                    result.error = "Có lỗi trong quá trình tạo cơ sở dữ liệu, vui lòng thử lại!"
                    res.status(status).send(result);
                } else {
                    status = 200;
                    result.status = status;
                    result.id = id;
                    var transporter = nodemailer.createTransport({
                        service: 'gmail',
                        auth: {
                            user: 'noreply.yuh@gmail.com',
                            pass: 'yuh@12345'
                        }
                    });

                    var mailOptions = {
                        from: 'noreply@gmail.com',
                        to: `${data.email}`,
                        subject: 'Register successfully - Welcome to YUH',
                        html: `<b>Hello ${data.name}</b>, welcome to YUH<br>Here your account, please reset your password<br><br>Username: ${data.username}<br>Password: ${pass}`
                    };

                    transporter.sendMail(mailOptions, function(error, info) {
                        if (error) {
                            console.log(error);
                        } else {
                            console.log('Email sent: ' + info.response);
                        }
                    });
                    res.status(status).send(result);
                }
            });
        } else {
            status = 406;
            result.status = status;
            result.error = msg
            res.status(status).send(result);
        }
    })
}
exports.verifyToken = (req, res, next) => {
    var bearerHeader = req.headers.authorization
    if (bearerHeader == undefined) {
        return res.status(500).send({ status: 500, message: "Token không hợp lệ" })
    } else
        var token = bearerHeader.split(" ")
    jwt.verify(token[1], process.env.SECRET_KEY, (err, user) => {
        if (err) {
            return res.status(400).send({ status: 400, message: err.message })
        } else {
            permissionController.getPermission(user, req, (err, data) => {
                if (err != null) {
                    return res.status(500).send({ status: 500, message: err.message })
                } else if (data == null) {
                    return res.status(400).send({ status: 400, message: "Người dùng không được phép thực thi" })
                } else if (data) {
                    req.currentUser = user
                    next()
                } else {
                    return res.status(500).send({ status: 500, message: "ERROR" })
                }
            })
        }
    })

}

exports.updateInfo = (req, res) => {
    var currentUser = req.currentUser
    var data = req.body
    new Promise(resolve => {
        if (data.password != undefined) {
            bcrypt.hash(data.password, 10, (err, pass) => {
                if (err)
                    return res.status(400).send({ status: 400, message: "Cannot update your password" })
                data.password = pass
                resolve(data)
            })
        } else {
            resolve(data)
        }
    }).then(data => {
        UserDB.updateUser({ _id: currentUser._id }, data, (err, user) => {
            if (err) {
                return res.status(400).send({ status: 400, message: err })
            }
            if (user) {
                user = data
                postDB.updatePost({ "creator._id": currentUser._id }, { name: user.name, avatar: user.avatar }, (err, post) => {})
                cmtDB.updateComment({ userID: currentUser._id }, { name: user.name, avatar: user.avatar }, (err, cmt) => {})
                return res.status(200).send({ status: 200, message: "Updated successfully", data: user })
            } else {
                return res.status(400).send({ status: 400, message: "Updated failed" })
            }
        })
    })
}

exports.updateInfoAny = async(req, res) => {
    var userID = req.query.userID
    var data = req.body
    new Promise(resolve => {
        if (data.password != undefined) {
            bcrypt.hash(data.password, 10, (err, pass) => {
                if (err)
                    return res.status(400).send({ status: 400, message: "Cannot update your password" })
                data.password = pass
                resolve(data)
            })
        } else {
            resolve(data)
        }
    }).then(data => {
        UserDB.updateUser({ _id: userID }, data, (err, user) => {
            if (err) {
                return res.status(400).send({ status: 400, message: err })
            }
            if (user) {
                user = data
                postDB.updatePost({ "creator._id": userID }, { name: user.name, avatar: user.avatar }, (err, post) => {})
                cmtDB.updateComment({ userID: userID }, { name: user.name, avatar: user.avatar }, (err, cmt) => {})
                return res.status(200).send({ status: 200, message: "Updated successfully", data: user })
            } else {
                return res.status(400).send({ status: 400, message: "Updated failed" })
            }
        })
    })
}