const express = require('express');
const router = express.Router();
const postController = require('../controllers/postController')
const authController = require('../controllers/authController');

router.post("/create", authController.verifyToken, postController.create)
router.get("/any", authController.verifyToken, postController.getPosts)
router.get("/any_by_id", authController.verifyToken, postController.getPostsById)

module.exports = router